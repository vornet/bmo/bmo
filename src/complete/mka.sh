#!/bin/bash

_saturnin__sccompgen_handle() {
    case $_SATURNIN_SCCOMPGEN__POS:$_SATURNIN_SCCOMPGEN__PREVIOUS in
        0:mka)
            echo -o -f -l --override --force --list
            pybmo mka -l
            ;;
        1:-f)       echo -o --override; bmo mka -l ;;
        1:--force)  echo -o --override; bmo mka -l ;;
    esac
}
