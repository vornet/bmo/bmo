#!/bin/bash

_saturnin__sccompgen_handle() {
    case $_SATURNIN_SCCOMPGEN__POS:$_SATURNIN_SCCOMPGEN__PREVIOUS in
        0:kb)
            echo show
            echo next
            echo home
            echo layout
            ;;
        1:show)     true ;;
        1:next)     true ;;
        1:home)     true ;;
        1:layout)   pybmo conf kb.layout ;;
    esac
}
