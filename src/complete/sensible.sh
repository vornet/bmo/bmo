#!/bin/bash

lots_stuff() {
    pybmo sensible -L
    pybmo sensible -L | cut -d. -f2- | sort | uniq
}

_saturnin__sccompgen_handle() {
    case $_SATURNIN_SCCOMPGEN__POS:$_SATURNIN_SCCOMPGEN__PREVIOUS in
        0:sensible)             echo '-a -l -L -s --all --list --list-roles --select'
                                lots_stuff ;;
        1:-a|1:--all)           bmo sensible -L ;;
        1:-L|1:--list-roles)    echo "gui tty" ;;
        1:-s|1:--select)        lots_stuff ;;
    esac
}
