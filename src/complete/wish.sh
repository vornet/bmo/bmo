#!/bin/bash

both_types() {
    {
        pybmo conf -K wish.bookmark
        pybmo conf -K wish.query
    } | sort | uniq
}

queries() {
    pybmo conf -K wish.query \
      | sort | uniq
}

browsers() {
    pybmo sensible -l browser \
      | cut -d: -f2 \
      | sort | uniq
}

_saturnin__sccompgen_handle() {
    local sopts=" -1 -a -B -Q -b -l -q "
    local lopts=" --choose-browser --choose-query --always-run --limit --browser --query "
    local cmds="-f -c -C --file --primary-clipboard --clipboard --secondary-clipboard"
    local pos=$_SATURNIN_SCCOMPGEN__POS
    local prev=$_SATURNIN_SCCOMPGEN__PREVIOUS
    local opts_m="${sopts/ $prev /}${lopts/$prev/}"
    case $pos:$prev in
        0:wish)
            echo "$sopts $lopts $cmds"
            both_types
            ;;
        1:-1)                   echo "$opts_m"; echo "$cmds"; both_types ;;
        1:--first)              echo "$opts_m"; echo "$cmds"; both_types ;;
        1:-a)                   echo "$opts_m"; echo "$cmds"; both_types ;;
        1:--always-run)         echo "$opts_m"; echo "$cmds"; both_types ;;
        1:-B)                   echo "$opts_m"; echo "$cmds"; both_types ;;
        1:--choose-browser)     echo "$opts_m"; echo "$cmds"; both_types ;;
        1:-Q)                   echo "$opts_m"; echo "$cmds" ;;
        1:--choose-query)       echo "$opts_m"; echo "$cmds" ;;
        1:-b)                   echo "$opts_m"; echo "$cmds"; browsers ;;
        1:--browser)            echo "$opts_m"; echo "$cmds"; browsers ;;
        1:-q)                   echo "$opts_m"; echo "$cmds"; queries ;;
        1:--query)              echo "$opts_m"; echo "$cmds"; queries ;;
        *:-c|*:-C|*:-*clipboard) ;;
        *:-f|*:--file)          compgen -f ;;
    esac
}
