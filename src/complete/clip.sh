#!/bin/bash

_saturnin__sccompgen_handle() {
    case $_SATURNIN_SCCOMPGEN__POS:$_SATURNIN_SCCOMPGEN__PREVIOUS in
        0:clip)
            echo ls
            echo lsh
            echo save
            echo load
            echo open
            echo cat
            echo rm
            echo clean
            ;;
        1:ls)     : ;;
        1:lsh)    : ;;
        1:save)   echo "-l -1 -2 -c -" ;;
        1:load)   echo "-l -1 -2 -c" ;;
        1:open)   : ;;
        1:cat)    echo "-l -1 -2 -c" ;;
        1:rm)     : ;;
        1:clean)  : ;;
        2:-l)     echo "-1 -2 -c -" ;;
        *:-1)     : ;;
        *:-2)     : ;;
        *:-c)     : ;;
        *:-)      : ;;
    esac
}
