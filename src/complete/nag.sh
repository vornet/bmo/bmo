#!/bin/bash

nags() {
    pybmo conf -S nag.ini \
      | grep '^nag\.def\.' \
      | cut -d. -f3- \
      | sort \
      | uniq
}

streams() {
    pybmo conf -K "nag.stream" \
      | sort \
      | uniq
}

_saturnin__sccompgen_handle() {
    case $_SATURNIN_SCCOMPGEN__POS:$_SATURNIN_SCCOMPGEN__PREVIOUS in
        0:nag|1:-n)
            echo "-n -l -L -r -R"
            nags
            streams | sed "s/^/@/"
            ;;
    esac
}
