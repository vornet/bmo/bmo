#!/bin/bash

_saturnin__sccompgen_handle() {
    case $_SATURNIN_SCCOMPGEN__POS:$_SATURNIN_SCCOMPGEN__PREVIOUS in
        0:au)
            echo up
            echo down
            echo flip
            echo mute
            echo query
            echo unmute
            ;;
        1:up)       true ;;
        1:down)     true ;;
        1:flip)     true ;;
        1:mute)     true ;;
        1:unmute)   true ;;
        1:query)    echo is_muted ;;
        2:is_muted) true ;;
    esac
}
