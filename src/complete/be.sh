#!/bin/bash

_saturnin__sccompgen_handle() {
    case $_SATURNIN_SCCOMPGEN__POS:$_SATURNIN_SCCOMPGEN__PREVIOUS in
        0:be)
            echo --move
            echo --leave
            echo --lsspots
            echo --lssxprs
            echo --lsaxes
            echo --stat
            bmo be --lssxprs
            ;;
        1:--move)    bmo be --lsspots ;;
        1:--leave)   bmo be --lsaxes ;;
        1:--lsaxes)  true ;;
        1:--lsspots) bmo be --lsaxes ;;
        1:--)        bmo be --lssxprs ;;
    esac
}
