BMO-MENU - easy and versatile menu manager
========

*bmo-menu* is universal menu manager.  You define what appears in the
menu, how the selection is invoked and what is done with your choice
and *bmo-menu* links it all together and adds sugar like logging and
de-duplicated history.


General principle
-----------------

There are three components -- all of them are simple UNIX-like shell
commands -- that compose a BMO menu:

    .-----------.    .----------.    .----------.
    | generator | -> | selector | -> | consumer |
    '-----------'    '----------'    '----------'

Invoking a menu just links all of these components into a pipe, so:

 *  *generator* is any command that will produce list of menu items
    on stdout.

 *  *selector* is the UI command that will accept list of menu items
    on stdin, present user with the menu and output the selected item
    to stdout.

    Default *selector* for bmo is `bmo dmenu`, which is a wrapper
    around `dmenu`.  In this selector, each line of stdin is presented
    as one item.

 *  *consumer* is any command that will accept the selected item
    on stdout.

There are two more modes that can simplify common scenarios.

You can simply omit the *generator* and just specify list of items
directly in the INI file:

    line1    .----------.    .----------.
    line2 -> | selector | -> | consumer |
    line2    '----------'    '----------'

Or you can enable *history* mode, which will keep history of selected
items and use them in place of *generator*.

        .----------.        .----------.
    .-> | selector | --+--> | consumer |
    |   '----------'   |    '----------'
    |                  |
    |   .----------.   |
    '---| .history |---'
        '----------'

The history mode assumes that the *selector* can "produce" items that
were not on stdout---as the initial list is inevitably empty.  The
default *dmenu*-based selector can do this.


Defining menus
--------------

To define menu, create *menu.ini* in *.config/bmo* sub-directory of your
home directory.  Each menu is defined by own INI section called
`menu.def.FOO` where `FOO` would be the menu name.

Each menu must define at least two things:

 *  `consumer` key defining the consumer command.

 *  one of following:

     *  `generator` key with the generator command,
     *  one or more `item` keys,
     *  or `mode` key set to `history`.

Optionally, you can define

 *  `selector` key defining the selector command,

 *  `selector_args` with any extra arguments that bmo-menu will add when
    calling selector,

 *  `histsize`, which, in *history* mode will set limit to number of
    remembered items.

 *  a `menu._default_` section which will define defaults for other
    menus.


Example menu
------------

We'll explain by example:

    [menu._default_]
        selector = dmenu

    [menu.def.home]
        generator = ls -1 -t Downloads
        selector_args = -l 30
        consumer = xargs -Ipath thunar "path"

    [menu.def.launch]
        mode = history
        histsize = 1000
        consumer = sh - &

    [menu.def.status]
        item = gone
        item = afk
        item = back
        consumer = xargs set_status

Above file enables three commands:

 *  `bmo menu home` will get simple list of your files in Downloads
    directory, sorted from latest to oldest.  Selecting item will open
    it using Thunar (XFCE's file manager).

 *  `bmo menu launch` will start with empty list, so you will have to
    type first command.  However, the command is remembered so that next
    time you can re-use it.  History is de-duplicated automatically,
    appears in last-used order and in this case, we have selected that
    only 1000 last commands are remembered (default is no limit).

 *  `bmo menu status` will start with precisely the three items defined
    in the body of the .ini file.  The consumer in this case is fictional,
    since there's no easy example of changing status, but you get
    the idea.

In all three cases, the application invoked to present you the menu is
`dmenu`.  You can use other implementation but it does need to have
the same interface (take items as lines on stdin and output selection
on stdout).


Further examples
================

There's more to it, but this document is only meant as 101; full reference
manual will be available at some point.  In the meantime, you can look at
[author's own menu.ini][1] to get some inspiration:

  [1]: https://gitea.vornet.cz/netvor/mydots/src/branch/master/dotfiles/config/bmo/menu.ini
