BMO-NAG - a sane opportunistic nagging system
=======

*bmo-nag* will not nag you by itself.  The actual execution of any
nagging is up to you.  The main idea of *bmo-nag* is to keep the nags
effective by

 *  giving you exclusive control of when the nags can appear,

 *  limiting rate at which same nag re-appears.

In other words, *bmo-nag* implements "on-demand nagging".

The aim is to prevent from "nag blindness":

> If nags like TODO's or broken things appear in times when you
> can't (or don't want to) act on them, your brain will inevitable
> learn to just completely ignore whatever is the window, app, fridge
> or wall where these nags appear.

So with *bmo-nag*, it's your job to find the best moments in your day
when nags should be *given opportunity* to show up.  On login?  On
VPN connect?  Every hour?  When new e-mail is received?


Defining NAGs
-------------

NAGs are dynamic text messages defined by user in such way that:

 *  You can use any software (or combination thereof) that's capable of
    printing text to standard output to generate them.

 *  You will define minimum re-appear delay so that no matter when
    *bmo-nag* is called, a nag won't appear too often.


Get started
-----------

To define NAG, create *nag.ini* in *.config/bmo* sub-directory of your
home directory.  We'll explain by example:

    [nag.stream]
        master = timenow overdue

    [nag.def.timenow]
        command = date
        expiry = 10 minutes

    [nag.def.overdue]
        command = task rc.verbose=nothing list +OVERDUE -ACTIVE
        expiry = 30 minutes

The above file defines two NAGs: `timenow` and `overdue`, showing current
date/time, and output from *task* application.  It also defines order for
them: `timenow` first, `overdue` next.


### How it works ###

This means that now you can call one of following commands:

    bmo nag timenow
    bmo nag overdue
    bmo nag

Either of first two will simply display the respective nag generated by
respective command.  Very straightforward.

The third command is what's interesting:  First, since `timenow` is
first on the `master` stream, *bmo-nag* will try to display `timenow`.
If both following conditions are fulfilled:

 *  the last time `timenow` has been displayed is no less than ten
    minutes ago,

 *  and the nag command does produce at least some text,

the nag is displayed and *bmo-nag* exits.  If one of the conditions above
was *not* fulfilled, *bmo-nag* will continue to the next item on stream
(`overdue`) and repeat the same logic.

If with above INI file we scheduled `bmo nag` to run every minute starting
midnight, and in the meantime, called `bmo nag overdue` exactly at 0:17:

    0:00    timenow     # overdue postponed
    0:01    overdue     # overdue shown
    0:10    timenow     # 10 minutes since last appearance
    0:17    overdue     # because we called it
    0:20    timenow     # another 10 minutes for 'timenow'
    0:30    timenow     # ...
    0:40    timenow     # 10 minutes since last appearance
    0:47    overdue     # 30 minutes since last appearance
    0:50    timenow     # ...
    ...

This means that you can really take opportunistic approach when finding
the right moments "to be nagged". As long as you can find the good
'spots' and balance the timing right, nags will remain effective: they
will find their way to your attention but never too often to force you
to ignore them or turn them off.


Details
-------

The above example was the minumum to show how to get started, but there's
more of what you can configure.

 *  The `nag.stream` section should contain one key per each stream; each
    key should contain a space-separated list of nags ordered by priority.

 *  A nag stream can be selected by calling `bmo nag @STREAM`; if called
    without arguments, the default stream name is either the value of
    `BMO_NAG_STREAM` environment variable, or `master`, if `BMO_NAG_STREAM`
    is unset or empty.

 *  Each of these nags then has to have respective section called
    `nag.def.NAME`, where `NAME` is the string you used in `master` value.

Following keys are valid in nag definition:

 *  `command` - command to create the nag text

 *  `expiry` - nag expiry; ie. minimum time since last showing after which
    the nag may re-appear.

    Format is `N UNIT` where N must be positive integer and UNIT must
    be one of following:

        y yr yrs year years
        mo mos month months
        w wk wks week weeks
        d dy dys day days
        h hr hrs hour hours
        m min mins minute minutes

 *  `notify-urgency` (optional) - urgency to use when sending nag as
    a desktop notification (uses *notify-send*)

    Can be `low`, `normal` (default) or `critical`.

 *  `term-color` - color when printing to terminal.

    Can be: `black`, `blue`, `cyan`, `green`, `lblack`, `lblue`, `lcyan`,
    `lgreen`, `lmagenta`, `lred`, `lwhite`, `lyellow`, `magenta`, `none`,
    `red`, `white` or `yellow`.

    By default, no color codes are sent.

