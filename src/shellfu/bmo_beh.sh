#!/bin/bash
#shellcheck disable=SC1090

. "$(sfpath)" || exit 3

shellfu import pretty

#
# bmo-be handler helper functions
#
# This module is loaded before handler code is executed, so the
# functions and variables here can be used to help control handler
# code flow.
#

#
# Axis being currently acted on
#
BMO_BEH__AXIS=${BMO_BEH__AXIS:-}

#
# Event being currently executed
#
BMO_BEH__EVENT=${BMO_BEH__EVENT:-}

#
# Sigil of axis currently acted on
#
BMO_BEH__SIGIL=${BMO_BEH__SIGIL:-}

#
# Spot being currently activated
#
BMO_BEH__SPOT=${BMO_BEH__SPOT:-}

#
# SXPR of spot being currently activated
#
BMO_BEH__SXPR=${BMO_BEH__SXPR:-}

bmo_beh__abort() {
    #
    # Abort current handler code with message $1
    #
    warn -l "$@"
    exit 1
}

bmo_beh__if() {
    #
    # Do if test passes
    #
    # Usage:
    #     bmo_beh__if [-w] TEST.. [--] CMD [ARGS..]
    #     bmo_beh__if [-w] TEST.. --then CMD [ARGS..] [--else CMD [ARGS...]]
    #
    # Command CMD with all its ARGS will be launched only if all
    # provided TESTs pass.  TEST composes of two arguments: test
    # name, which consists of single dash sign or a plus sign
    # followed by an alphabetical character designating test type,
    # and test argument.  Possible tests are:
    #
    #     -p PID    process with identifier PID is running,
    #     -P PNAME  process with name PNAME is running,
    #     -f FUNC   function FUNC returns zero.
    #     -c CODE   shell code CODE
    #
    # Additionally, each of tests can be provided with plus sign
    # instead of dash (eg. `+P foo`), which reverts the boolean
    # value of the test, ie.
    #
    local __bmo_beh__cmd_t=()
    local __bmo_beh__cmd_e=()
    local __bmo_beh__r=opts
    local __bmo_beh__tests=()
    local __bmo_beh__warn=false
    local __bmo_beh__tes=0
    local __bmo_beh__has_t=false
    local __bmo_beh__has_e=true
    while true; do case $__bmo_beh__r:$1 in
        opts:-w)        __bmo_beh__warn=true; shift ;;
        opts:*)         __bmo_beh__r=tests ;;
        tests:-P)       __bmo_beh__tests+=( t_pname 0 "$2" ); shift 2 ;;
        tests:-c)       __bmo_beh__tests+=( t_code  0 "$2" ); shift 2 ;;
        tests:-f)       __bmo_beh__tests+=( t_func  0 "$2" ); shift 2 ;;
        tests:-p)       __bmo_beh__tests+=( t_pid   0 "$2" ); shift 2 ;;
        tests:+P)       __bmo_beh__tests+=( t_pname 1 "$2" ); shift 2 ;;
        tests:+c)       __bmo_beh__tests+=( t_code  1 "$2" ); shift 2 ;;
        tests:+f)       __bmo_beh__tests+=( t_func  1 "$2" ); shift 2 ;;
        tests:+p)       __bmo_beh__tests+=( t_pid   1 "$2" ); shift 2 ;;
        tests:--then-c) __bmo_beh__r=cmd_T; shift ;;
        tests:--then)   __bmo_beh__r=cmd_t; shift ;;
        tests:--)       __bmo_beh__r=cmd_t; shift ;;
        tests:-*)       __bmo_beh__ue "syntax error"; return 2 ;;
        tests:*)        __bmo_beh__r=cmd_t ;;
        cmd_t:)         break ;;
        cmd_t:--else)   __bmo_beh__r=cmd_e; shift ;;
        cmd_t:--else-c) __bmo_beh__r=cmd_E; shift ;;
        cmd_t:*)        __bmo_beh__cmd_t+=("$1"); shift ;;
        cmd_T:)         break ;;
        cmd_T:*)        __bmo_beh__cmd_t=(eval "$1"); shift ;;
        cmd_e:)         break ;;
        cmd_e:*)        __bmo_beh__cmd_e+=("$1"); shift ;;
        cmd_E:)         break ;;
        cmd_E:*)        __bmo_beh__cmd_e=(eval "$1"); shift ;;
        *)  break ;;
    esac done
    debug -v __bmo_beh__tests __bmo_beh__cmd_t __bmo_beh__cmd_e
    test "${#__bmo_beh__cmd_t[@]}" -gt 0 \
     && __bmo_beh__has_t=true
    test "${#__bmo_beh__cmd_e[@]}" -gt 0 \
     && __bmo_beh__has_e=true
    $__bmo_beh__has_t \
     || { __bmo_beh__ue "no CMD?"; return 2; }
    __bmo_be__dotests "${__bmo_beh__tests[@]}"; __bmo_beh__tes=$?
    debug -v __bmo_beh__tes
    if test $__bmo_beh__tes -gt 1; then
        warn "bad exit status from tests: $__bmo_beh__tes > 1"
        return 3
    elif test $__bmo_beh__tes -eq 0; then
        "${__bmo_beh__cmd_t[@]}"
    elif $__bmo_beh__has_e; then
        "${__bmo_beh__cmd_e[@]}"
    elif $__bmo_beh__warn; then
        warn "command not launched: ${__bmo_beh__cmd_t[*]}"
    fi
}

bmo_beh__mention() {
    #
    # Send low-priority desktop notification with message lines $@..
    #
    # Usage:
    #
    #     bmo_beh__mention LINE..
    #
    __bmo_beh__notify low "$@"
}

bmo_beh__notify() {
    #
    # Send desktop notification with message lines $@..
    #
    # Usage:
    #
    #     bmo_beh__notify LINE..
    #
    __bmo_beh__notify normal "$@"
}

bmo_beh__scream() {
    #
    # Send critical-priority desktop notification with message lines $@..
    #
    # Usage:
    #
    #     bmo_beh__scream LINE..
    #
    __bmo_beh__notify critical "$@"
}

__bmo_be__dotests() {
    #
    # Run all tests from $@
    #
    local __bmo_be__ttype
    local __bmo_be__toes
    local __bmo_be__targ
    local __bmo_be__tres
    while test $# -gt 0; do
        __bmo_be__ttype=$1; __bmo_be__toes=$2; __bmo_be__targ=$3
        debug -v __bmo_be__ttype __bmo_be__toes __bmo_be__targ
        shift 3 || {
            warn "bad parity"
            return 3
        }
        case $__bmo_be__ttype in
            t_code)
                bash -n <<<"$__bmo_be__targ" || {
                    warn "bad syntax: $__bmo_be__targ"
                    return 3
                 }
                ( eval "$__bmo_be__targ"; ); __bmo_be__tres=$?
                ;;
            t_func)
                test "$(type -t "$__bmo_be__targ")" == "function" || {
                    __bmo_beh__ue "no such function: $__bmo_be__targ()"
                    return 3
                }
                ( "$__bmo_be__targ"; ); __bmo_be__tres=$?
                ;;
            t_pname)
                pidof "$__bmo_be__targ" >/dev/null; __bmo_be__tres=$?
                ;;
            t_pid)
                ps -q "$__bmo_be__targ"; __bmo_be__tres=$?
                ;;
        esac
        debug -v __bmo_be__tres
        test "$__bmo_be__tres" -eq "$__bmo_be__toes" || return 1
    done
    return 0
}

__bmo_beh__notify() {
    #
    # Send notification with urgency $1 and message lines $2..
    #
    local urg=$1; shift
    local body
    local line
    test $# -gt 0 || {
        __bmo_beh__ue "no LINE?"
    }
    for line in "$@"; do
        body+="$line"$'\n'
    done
    notify-send \
        -u "$urg" \
        -a "bmo-be" \
        "event '$BMO_BEH__EVENT' for $BMO_BEH__SXPR" \
        "$body"
}

__bmo_beh__ue() {
    #
    # Print usage error $1
    #
    local msg=$1
    warn "bad usage: $msg"
}
