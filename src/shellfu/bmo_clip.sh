#!/bin/bash
#shellcheck disable=SC1090

shellfu import pretty

#
# X Clipboard name to use 
#
BMO_CLIP__CLIPNAME=${BMO_CLIP__CLIPNAME:-primary}

#
# 'true' to add final newline if missing
#
BMO_CLIP__FIX_NEWLINE=${BMO_CLIP__FIX_NEWLINE:-true}

#
# Source of clip
#
BMO_CLIP__SOURCE=${BMO_CLIP__SOURCE:-}

#
# Main clip storage directory
#
BMO_CLIP__STORAGE=${BMO_CLIP__STORAGE:-}


bmo_clip__cat() {
    #
    # Print desired clip and fix newline if needed
    #
    __bmo_clip__get | __bmo_clip__fixnl
}

bmo_clip__load() {
    #
    # Load single clip of choice
    #
    local name  # chosen clip name
    name=$(__bmo_clip__pick_clip) || return 1
    < "$BMO_CLIP__STORAGE/$name" xclip -i -selection "$BMO_CLIP__CLIPNAME"
}

bmo_clip__ls() {
    #
    # List clips with MIME types and hints
    #
    local ft    # MIME filetype of a clip
    local hint  # our hint (preview)
    local name  # clip name
    find "$BMO_CLIP__STORAGE" -name "*.clip" 2>/dev/null \
      | sort -r \
      | while read -r name;
        do
            ft=$(file -b -i "$name" | cut -d\; -f1)
            case $ft in
                text/*)
                    hint=$(head -c 80 "$name" | awk 1 ORS='↵')
                    ;;
                *)
                    hint=$(head -c 16 "$name" | hexdump -C | head -1)
                    ;;
            esac
            echos "$(basename "$name") • $ft • $hint"
        done \
      | grep -a . || {
            warn "clipboard storage is empty"
            return 1
        }
}

bmo_clip__lspaths() {
    #
    # List only clip paths
    #
    ls "$BMO_CLIP__STORAGE/"*.clip
}

bmo_clip__open() {
    #
    # Open single clip using bmo sensible
    #
    # Since we're dealing with X clipboard, we'll assume GUI is
    # expected.  Also we're not detecting the type (so far?),
    # the editor will have to deal with any kind of data that
    # could be there.
    #
    #TODO: can we do better?
    local name  # chosen clip name
    name=$(__bmo_clip__pick_clip) || return 1
    bmo sensible gui.editor "$BMO_CLIP__STORAGE/$name"
}

bmo_clip__rm() {
    #
    # Remove single clip of choice
    #
    local name  # chosen clip name
    name=$(__bmo_clip__pick_clip) || return 1
    test -n "$name" || return 1
    rm -f "$BMO_CLIP__STORAGE/$name"
}

bmo_clip__rm_all() {
    #
    # Drop all clips
    #
    test -n "$BMO_CLIP__STORAGE" || die "storage directory is unset, aborting"
    test -d "$BMO_CLIP__STORAGE" || return 0
    find "$BMO_CLIP__STORAGE" -name "*.clip" -print0 | xargs -0 rm -f
    rmdir "$BMO_CLIP__STORAGE" 2>/dev/null | :
}

bmo_clip__save() {
    #
    # Save single clip
    #
    local path  # path to clip file to save into
    mkdir -p "$BMO_CLIP__STORAGE" || die "could not create directory for saving"
    path="$BMO_CLIP__STORAGE/$(date +%Y%m%d-%H%M%S.clip)"
    bmo_clip__cat > "$path"
    test -s "$path" || {
        rm "$path"
        warn "clipboard is empty, nothing saved"
        return 1
    }
}

#          #              a curseword will be added to one of your clips #
# INTERNAL # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#          #                               every time you skip this line #

__bmo_clip__fixnl() {
    #
    # Fix newline if needed
    #
    $BMO_CLIP__FIX_NEWLINE || { cat; return 0; }
    local lastchr
    local cache
    cache=$(mktemp -t bmo-clip.__bmo_clip__fixnl.XXXXXXXX)
    cat >"$cache"
    lastchr=$(<"$cache" tail -c1 | hexdump -e '"%02x"')
    cat "$cache"
    test "$lastchr" = '0a' || echo
    rm "$cache"
}

__bmo_clip__get() {
    #
    # Get clip from desired $BMO_CLIP__SOURCE
    #
    case $BMO_CLIP__SOURCE in
        xclip) xclip -o -selection "$BMO_CLIP__CLIPNAME" 2>/dev/null ;;
        stdin) cat ;;
    esac
}

__bmo_clip__pick_clip() {
    #
    # Have user pick clip using bmo-dmenu
    #
    bmo_clip__ls >/dev/null || return 1      # if it was empty
    bmo_clip__ls \
      | bmo dmenu \
      | cut -d\   -f 1 \
      | grep . || {
        warn "nothing chosen"
        return 1
    }
}
