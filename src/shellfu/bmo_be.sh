#!/bin/bash
#shellcheck disable=SC1090

. "$(sfpath)" || exit 3

shellfu import isa
shellfu import pretty

bmo_be__a2s() {
    #
    # Print sigil of axis $1
    #
    # Usage:
    #     bmo_be__a2s AXIS
    #
    local axis=$1
    test -n "$axis" || { __bmo_be__ue "no AXIS?"; return 2; }
    debug -v axis
    saturnin__conf -j -1 "be.def.axis.$axis.sigil"
}

bmo_be__aprop() {
    #
    # Print property $2 of axis $1
    #
    # Usage:
    #     bmo_be__aprop AXIS PROP
    #
    local axis=$1
    local prop=$2
    test -n "$axis" || { __bmo_be__ue "no AXIS?"; return 2; }
    test -n "$prop" || { __bmo_be__ue "no PROP?"; return 2; }
    debug -v axis prop
    saturnin__conf -j -1 "be.def.axis.$axis.$prop"
}

bmo_be__isa_axis() {
    #
    # True if $1 is an axis
    #
    # Usage:
    #     bmo_be__isa_axis THING
    #
    local thing=$1
    test -n "$thing" || { __bmo_be__ue "no THING?"; return 2; }
    bmo_be__lsaxes \
      | grep -qxFe "$thing"
}

bmo_be__isa_spot_on() {
    #
    # True if $2 is a spot on axis $1
    #
    # Usage:
    #     bmo_be__isa_spot_on AXIS THING
    #
    local axis=$1
    local thing=$2
    test -n "$axis"  || { __bmo_be__ue "no AXIS?"; return 2; }
    test -n "$thing" || { __bmo_be__ue "no THING?"; return 2; }
    bmo_be__lsspots "$axis" \
      | grep -qxFe "$axis:$thing"
}

bmo_be__leave() {
    #
    # De-activate current spot on axis $1
    #
    # Usage:
    #     bmo_be__leave AXIS
    #
    local Axis=$1
    local OldSpot
    test -n "$Axis"          || { __bmo_be__ue "no AXIS?"; return 2; }
    bmo_be__isa_axis "$Axis" || { __bmo_be__ue "invalid AXIS"; return 2; }
    OldSpot=$(__bmo_be__rstate) || {
        think "nothing to do; already left: $Axis"
        return 0
    }
    think "leaving spot: $Axis:$OldSpot"
    __bmo_be__erun leave "$OldSpot" || return 3
    __bmo_be__wsovw
}

bmo_be__lsaxes() {
    #
    # List available axes
    #
    # Usage:
    #     bmo_be__lsaxes
    #
    saturnin__conf -j -S be.ini \
      | grep -xE 'be\.def\.axis\.\w+' \
      | cut -d. -f4- \
      | sort \
      | uniq \
      | grep .
}

bmo_be__lsspots() {
    #
    # List available spots on axis $1 or all
    #
    # Usage:
    #     bmo_be__lsspots [AXIS]
    #
    local axis=$1
    test -n "$axis" || axis='\w+'
    saturnin__conf -j -S be.ini \
      | grep -xE 'be\.def\.spot\.'"$axis"':\w+' \
      | cut -d. -f4 \
      | sort \
      | uniq \
      | grep .
}

bmo_be__move() {
    #
    # Activate spot $2 on axis $1
    #
    # Usage:
    #     bmo_be__move AXIS SPOT
    #
    local Axis=$1
    local NewSpot=$2
    local OldSpot
    local shape
    test -n "$Axis"          || { __bmo_be__ue "no AXIS?"; return 2; }
    test -n "$NewSpot"       || { __bmo_be__ue "no SPOT?"; return 2; }
    bmo_be__isa_axis "$Axis" || { __bmo_be__ue "invalid AXIS"; return 2; }
    bmo_be__isa_spot_on "$Axis" "$NewSpot" \
     || { __bmo_be__ue "no such SPOT on AXIS: $NewSpot on $Axis"; return 2; }
    think "activating spot: $Axis:$NewSpot"
    OldSpot=$(__bmo_be__rstate)
    shape=$(saturnin__conf -j -1 "be.def.spot.$Axis:$NewSpot".shape)
    test -n "$shape" || shape="stay"
    debug -v shape
    case $shape in
        stay)
            test "$NewSpot" == "$OldSpot" && {
                think "nothing to do; already at: $Axis:$NewSpot"
                return 0
            }
            __bmo_be__erun check "$NewSpot" || return 3
            __bmo_be__erun leave "$OldSpot" || return 3
            __bmo_be__erun enter "$NewSpot" || return 3
            ;;
        bounce)
            __bmo_be__hhas spike "$NewSpot" \
             || { __bmo_be__ce "bounce spot with undefined 'spike' handler: $Axis:$NewSpot" \
                               "be.def.spot.$Axis:$NewSpot"; return 2; }
            __bmo_be__erun check "$NewSpot" || return 3
            __bmo_be__erun leave "$OldSpot" || return 3
            __bmo_be__erun enter "$NewSpot" || return 3
            __bmo_be__erun spike "$NewSpot" || return 3
            __bmo_be__erun leave "$NewSpot" || return 3
            __bmo_be__erun enter "$OldSpot" || return 3
            ;;
        *)
            __bmo_be__ue "invalid shape: $shape of spot $Axis:$NewSpot"
    esac
    __bmo_be__wsovw
}

bmo_be__s2a() {
    #
    # Print axis for sigil $1
    #
    # Usage:
    #     bmo_be__s2a SIGIL
    #
    local sigil=$1  # sigil of interest
    local axis      # each axis
    test -n "$sigil" || { __bmo_be__ue "no SIGIL?"; return 2; }
    debug -v sigil
    for axis in $(bmo_be__lsaxes); do
        debug -v axis
        saturnin__conf -j -1 "be.def.axis.$axis.sigil" \
          | grep -qxFe "$sigil" \
         && { echo "$axis"; return 0; }
    done
    return 1
}

bmo_be__sprop() {
    #
    # Print property $3 of spot $2 on axis $1
    #
    # Usage:
    #     bmo_be__sprop AXIS SPOT PROP
    #
    local axis=$1
    local spot=$2
    local prop=$3
    test -n "$axis" || { __bmo_be__ue "no AXIS?"; return 2; }
    test -n "$spot" || { __bmo_be__ue "no SPOT?"; return 2; }
    test -n "$prop" || { __bmo_be__ue "no PROP?"; return 2; }
    debug -v axis spot prop
    saturnin__conf -j -1 "be.def.spot.$axis:$spot.$prop"
}

bmo_be__state() {
    #
    # Show state of axis $1
    #
    # Usage:
    #     bmo_be__state AXIS
    #
    local Axis=$1
    test -n "$Axis" || { __bmo_be__ue "no AXIS?"; return 2; }
    __bmo_be__rstate
}

#          #                            your handlers will become borken #
# INTERNAL # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#          #                            if you use code beyond this line #


#
# Dry mode (don't do anything, just talk about it)
#
__BMO_BE__DRY=${__BMO_BE__DRY:-false}


__bmo_be__ce() {
    #
    # Print config error $1 and hint according to section $2
    #
    local msg=$1        # usage error
    local section=$2    # section (showing usage thereof)
    local patt          # each pattern to mentor on
    local patts=()      # acceptable usage patterns
    warn "bad config: [$section]: $msg"
    case $section in
        be.def.spot.*)  patts=(
            $'  shape = bounce\n  spike = some_command'
            $'  enter = entering_command\n  leave = leaving_command'
        ) ;;
    esac
    for patt in "${patts[@]}"; do
        warn -l "" "# usage:" "#" "[$section]" "$patt"
    done
}

__bmo_be__dpath() {
    #
    # Dereference user data path for key $1
    #
    local key=$1    # key (relative path in store)
    local store     # store for us
    local path      # final path
    store=$(saturnin__get data-home) || {
        saturnin__bug "data-home path not set"
        return 4
    }
    path="$store/be/$key"
    mkdir -p "$(dirname "$path")" || {
        warn "could not write to data-home path: $path"
        return 4
    }
    echo "$path"
}

__bmo_be__erun() {
    #
    # Run event $1 with spot $2 and axis $Axis
    #
    local event=$1
    local Spot=$2
    local Sigil
    Sigil=$(bmo_be__a2s "$Axis")
    __bmo_be__welog "${event^^}"
    case $event in
        check)
            __bmo_be__hrun check || {
                warn "check failed for: $Axis:$Spot"
                return 1
            }
            ;;
        enter)
            __bmo_be__hrun enter || {
                warn "could not enter spot: $Axis:$Spot"
                return 3
            }
            __bmo_be__wstate
            ;;
        spike)
            __bmo_be__hrun spike || {
                warn "could not spike spot: $Axis:$Spot"
                return 3
            }
            ;;
        leave)
            test "$Spot" == "-" && return 0
            __bmo_be__hrun leave || {
                warn "could not leave spot: $Axis:$Spot"
                return 3
            }
            __bmo_be__wstate "-"
            ;;
        *)
            saturnin__bug "unknown event: $event"
            ;;
    esac
    return 0
}

__bmo_be__hhas() {
    #
    # True if spot $NewSpot on axis $Axis has handler $1
    #
    local hevent=$1     # handler event
    local spot=$2       # spot for which check
    saturnin__conf -j "be.def.spot.$Axis:$spot.$hevent" \
     | grep -q .
}

__bmo_be__hrun() {
    #
    # Load, check and run handler code for spot $Spot
    #
    local hevent=$1     # handler event
    local hcode         # handler code
    local hes           # ^^ exit status
    local htmp          # ^^ temp
    local maxes=0       # maximum allowed exit status
    test "$hevent" == "check" && maxes=1
    debug -v hevent
    hcode=$(saturnin__conf -j "be.def.spot.$Axis:$Spot.$hevent")
    test -n "$hcode" || return 0
    debug -v hcode
    bash -n <<<"$hcode" || {
        warn "syntax error in handler code: $Spot.$hevent"
        return 3
    }
    $__BMO_BE__DRY && {
        local dry_lines=()
        dry_lines+=("dry: would run handler to: $Sigil$Spot.$hevent()")
        dry_lines+=("dry:   export BMO_BEH__AXIS=$Axis")
        dry_lines+=("dry:   export BMO_BEH__EVENT=$hevent")
        dry_lines+=("dry:   export BMO_BEH__SIGIL=$Sigil")
        dry_lines+=("dry:   export BMO_BEH__SPOT=$Spot")
        dry_lines+=("dry:   export BMO_BEH__SXPR=$Sigil$Spot")
        #shellcheck disable=SC2001
        dry_lines+=("$(sed "s/^/dry:   /" <<<"$hcode")")
        warn -l "${dry_lines[@]}"
        return 0
    }
    htmp=$(mktemp -d -t __bmo_be__hrun.htmp.XXXXXXXX)
    (
        cd "$htmp" || die "failed to chdir to handler temp"
        BMO_BEH__AXIS=$Axis \
        BMO_BEH__EVENT=$hevent \
        BMO_BEH__SIGIL=$Sigil \
        BMO_BEH__SPOT=$Spot \
        BMO_BEH__SXPR=$Sigil$Spot \
            bash <<<$'
            #!/bin/bash
            . "$(sfpath)" || exit 3
            shellfu import bmo_beh\n'"$hcode"
    ) >"$htmp/out" 2>"$htmp/err"; hes=$?
    echo "$hcode" > "$htmp/code"
    __bmo_be__whlog "$hevent" "$htmp" "$hes"
    __bmo_be__herrwarn "$htmp/err"
    __bmo_be__heswarn "$hes" "$maxes"
    rm -r "$htmp"
    return 0
}

__bmo_be__herrwarn() {
    #
    # Warn about error output
    #
    local efile=$1
    local lines=()
    test -s "$efile" || return 0
    debug -f efile
    mapfile -t lines <<<"$(sed "s/^/  ERR=|/" "$efile")"
    debug -v lines
    warn -l "handler code generated error output:" "${lines[@]}"
}

__bmo_be__heswarn() {
    #
    # Warn about exit status
    #
    local es=$1
    local maxes=$2
    test "$es" -gt "$maxes" || return 0
    debug -v es maxes
    warn -l "handler code exited with bad status: $hes" \
            " .. from event '$hevent' of spot '$Axis:$Spot'"
}

__bmo_be__rstate() {
    #
    # Read state of axis $1 or $Axis
    #
    local axis=${1:-$Axis}
    local path
    local state
    path=$(__bmo_be__dpath "state/$axis") \
     || die "data error"
    state=$(grep -s . "$path") \
     || state="-"
    echo "$state"
    test "$state" != "-"
}

__bmo_be__ue() {
    #
    # Print usage error $1 and hint according to FUNCNAME
    #
    local msg=$1    # usage message
    local parent=${FUNCNAME[1]}   # parent function (showing usage thereof)
    local patt      # each pattern to mentor on
    local patts=()  # acceptable usage patterns
    warn "bad usage: $parent(): $msg"
    case $parent in
        bmo_be__a2s)            patts=("AXIS") ;;
        bmo_be__aprop)          patts=("AXIS PROP") ;;
        bmo_be__move)           patts=("AXIS SPOT") ;;
        bmo_be__leave)          patts=("AXIS") ;;
        bmo_be__isa_axis)       patts=("THING") ;;
        bmo_be__isa_spot_on)    patts=("AXIS THING") ;;
        bmo_be__lsaxes)         patts=()  ;;
        bmo_be__lsspots)        patts=("[AXIS]") ;;
        bmo_be__s2a)            patts=("SIGIL") ;;
        bmo_be__sprop)          patts=("AXIS SPOT PROP") ;;
        bmo_be__state)          patts=("AXIS") ;;
    esac
    for patt in "${patts[@]}"; do
        warn "  usage: $parent $patt"
    done
}

__bmo_be__welog() {
    #
    # Write state operation $1 to state log
    #
    # Format is:
    #
    #    TIMESTAMP | Axis=AXIS Event=EVENT Spot=SPOT
    #
    # Where TIMESTAMP is ISO timestamp with second precision,
    # AXIS and SPOT is taken from $Axis and $Spot, and EVENT
    # is event name, which corresponds to handler name.
    #
    local event=$1
    local path
    local line
    path=$(__bmo_be__dpath "event.log") || {
        warn "data error"
        return 3
    }
    debug -v path
    line="$(date -Iseconds) | Axis=$Axis Event=$event Spot=$Spot"
    $__BMO_BE__DRY && return 0
    echo "$line" >> "$path" || {
        warn "event.log write error: $path"
        return 3
    }
}

__bmo_be__whlog() {
    #
    # Write handler relics
    #
    local event=$1
    local tmp=$2
    local es=$3
    local path
    local pfx
    local line
    path=$(__bmo_be__dpath "handler.log") || {
        warn "data error"
        return 3
    }
    debug -v path
    pfx="$(date -Iseconds) | Axis=$Axis Event=$event Spot=$Spot"
    {
        echo "$pfx START"
        while read -r line; do
            echo "$pfx CODE=|$line"
        done < "$tmp/code"
        while read -r line; do
            echo "$pfx OUT=|$line"
        done < "$tmp/out"
        while read -r line; do
            echo "$pfx ERR=|$line"
        done < "$tmp/err"
        echo "$pfx Es=$es"
        echo "$pfx END"
    } >> "$path" || {
        warn "handler.log write error: $path"
        return 3
    }
    return 0
}

__bmo_be__wsovw() {
    #
    # Update short state overview on all axis
    #
    local axis          # each axis
    local state         # current state of $axis
    local sigil         # sigil of $axis
    local imp_a         # T if $axis is important
    local imp_s         # T if $state is important
    local ovw_all       # overview, all axes
    local ovw_imp       # overview, important axes
    local path_all      # path for $ovw_all
    local path_imp      # path for $ovw_imp
    $__BMO_BE__DRY && return 0
    path_all=$(__bmo_be__dpath "short.state") \
     || die "data error"
    path_imp=$(__bmo_be__dpath "important.state") \
     || die "data error"
    for axis in $(bmo_be__lsaxes); do
        state=$(__bmo_be__rstate "$axis")
        sigil=$(bmo_be__aprop "$axis" "sigil")
        imp_a=F; imp_s=F
        isa__true "$(bmo_be__aprop "$axis" "important")" \
         && imp_a=T
        test "$state" != "-" \
         && isa__true "$(bmo_be__sprop "$axis" "$state" "important")" \
         && imp_s=T
        case $imp_a:$imp_s:$state in
            F:F:-)    continue ;;
            F:F:*)    : ;;
            T:F:*)    ovw_imp+=" $sigil$state" ;;
            F:T:*)    ovw_imp+=" $sigil$state" ;;
            T:T:*)    ovw_imp+=" $sigil$state" ;;
        esac
        ovw_all+=" $sigil$state"
    done
    echo -n "${ovw_all# }" > "$path_all"
    echo -n "${ovw_imp# }" > "$path_imp"
}

__bmo_be__wstate() {
    #
    # Write state on $Axis to be $1 or $Spot
    #
    local spot=${1:-$Spot}
    local path
    $__BMO_BE__DRY && return 0
    path=$(__bmo_be__dpath "state/$Axis") \
     || die "data error"
    debug -v Spot path
    echo "$spot" > "$path"
}
