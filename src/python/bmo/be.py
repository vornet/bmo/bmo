from __future__ import annotations
import datetime
import typing

import neaty.log as _LOG

from .vars import DATA_HOME

strict = False


class ParseError(ValueError):
    pass


class Event:

    @staticmethod
    def from_line(line: str) -> Event:
        # line='2019-04-05T12:13:50+02:00 | Axis=feel Event=CHECK Spot=day'
        stamp, rest = line.split(' | ', 1)
        fields = rest.split(' ')
        for field in fields:
            if field.startswith('Axis='):
                label, axis = field.split('=', 1)
            elif field.startswith('Event='):
                label, etype = field.split('=', 1)
            elif field.startswith('Spot='):
                label, spot = field.split('=', 1)
            else:
                raise ParseError('unknown field: %s' % field)
        return Event(stamp=stamp, axis=axis, etype=etype, spot=spot)

    def __init__(self, stamp: str, axis: str, etype: str, spot: str):
        self.stamp = stamp
        self.datetime = datetime.datetime.fromisoformat(stamp)
        self.axis = axis
        self.etype = etype
        self.spot = spot

    def __str__(self) -> str:
        return ('%s(stamp=%r, axis=%r,etype=%r,spot=%r)'
                % (self.__class__.__name__,
                   self.stamp, self.axis, self.etype, self.spot))


class Period:

    def __init__(self, e_enter: Event, e_leave: Event):
        if e_enter.axis != e_leave.axis:
            raise ParseError("axes don't match: %s != %s"
                             % (e_enter.axis, e_leave.axis))
        if e_enter.spot != e_leave.spot:
            raise ParseError("spots don't match: %s != %s"
                             % (e_enter.spot, e_leave.spot))
        if e_enter.etype != 'ENTER':
            raise ParseError("invalid starting event type: %s"
                             % e_enter.etype)
        if e_leave.etype != 'LEAVE':
            raise ParseError("invalid ending etype type: %s"
                             % e_enter.etype)
        self.axis = e_enter.axis
        self.spot = e_enter.spot
        self.e_enter = e_enter
        self.e_leave = e_leave
        self.duration = (
            e_leave.datetime - e_enter.datetime
        ).total_seconds() + 1

    def __str__(self):
        return ('%s(since=%r, axis=%r, spot=%r, duration=%.1fs)'
                % (self.__class__.__name__,
                   self.e_enter.stamp, self.axis, self.spot,
                   self.duration))

    def _clone(self, start: datetime.datetime, end: datetime.datetime
               ) -> Period:
        return Period(
            Event(
                stamp=start.isoformat(),
                axis=self.axis,
                spot=self.spot,
                etype='ENTER'
            ),
            Event(
                stamp=end.isoformat(),
                axis=self.axis,
                spot=self.spot,
                etype='LEAVE'
            ),
        )

    def _split2(self) -> tuple[Period, typing.Optional[Period]]:
        if self._fits_day():
            return (self, None)
        d1_start = self.e_enter.datetime
        d1_date = self.e_enter.datetime.date()
        d1_tzinfo = self.e_enter.datetime.tzinfo
        d1_end = datetime.datetime.combine(
            d1_date,
            datetime.time(23, 59, 59),
            tzinfo=d1_tzinfo)
        dr_start = datetime.datetime.combine(
            d1_date + datetime.timedelta(days=1),
            datetime.time(0, 0, 0),
            tzinfo=d1_tzinfo)
        dr_end = self.e_leave.datetime
        d1 = self._clone(d1_start, d1_end)
        dr = self._clone(dr_start, dr_end)
        return (d1, dr)

    def _fits_day(self) -> bool:
        return self.e_enter.datetime.date() == self.e_leave.datetime.date()

    def split_by_day(self) -> list[Period]:
        out = []
        head, tail = self._split2()
        out.append(head)
        while tail is not None:
            head, tail = tail._split2()
            out.append(head)
        return out


class EventGroup:

    def __init__(self, events: list[Event]):
        self.events = events
        self.periods = self._ev2per()
        self.flat_periods = []
        for p in self.periods:
            self.flat_periods.extend(p.split_by_day())

    def _ev2per(self) -> list[Period]:
        out = []
        curr: dict[tuple[str, str], Event] = {}
        if strict:
            say = _LOG.warn
        else:
            say = _LOG.debug
        for e in self.events:
            ekey = e.axis, e.spot
            if e.etype == 'ENTER':
                if ekey in curr:
                    say('ignoring double spot ENTER: %s overriden by %s'
                        % (curr[ekey], e))
                curr[ekey] = e
            if e.etype == 'LEAVE':
                if ekey not in curr:
                    say('ignoring stray spot LEAVE: %s' % e)
                    continue
                out.append(Period(curr[ekey], e))
                del curr[ekey]
        if curr:
            say('ignoring unfinished periods: %s' % curr)
        return out

    def _select(self,
                axis: str = None,
                spot: str = None,
                etype: str = None,
                ) -> EventGroup:
        selected = []
        conds = {}
        if axis is not None:
            conds['axis'] = axis
        if spot is not None:
            conds['spot'] = spot
        if etype is not None:
            conds['etype'] = etype
        for e in self.events:
            for key, value in conds.items():
                if getattr(e, key) == value:
                    selected.append(e)
        return EventGroup(events=selected)

    def all_axes(self) -> set[str]:
        out = set()
        for e in self.events:
            out.add(e.axis)
        return out

    def all_spots(self, axis: str) -> set[str]:
        out = set()
        for e in self.events:
            if e.axis == axis:
                out.add(e.axis)
        return out

    def group_by_axes(self) -> dict[str, EventGroup]:
        out = {}
        for axis in self.all_axes():
            out[axis] = self._select(axis=axis)
        return out

    def select_by_axis(self, axis: str) -> EventGroup:
        return self._select(axis=axis)

    def select_by_spot(self, axis: str, spot: str) -> EventGroup:
        return self._select(axis=axis, spot=spot)

    def select_by_etype(self, etype: str) -> EventGroup:
        return self._select(etype=etype)


class Log(EventGroup):

    @classmethod
    def from_file(cls, fname: str) -> Log:
        events = []
        with open(fname) as f:
            events = [Event.from_line(line.rstrip('\n'))
                      for line in f.readlines()]
        _LOG.debugv('lines', events)
        return cls(events)


try:
    log = Log.from_file(DATA_HOME + '/be/event.log')
except FileNotFoundError:
    log = Log([])
