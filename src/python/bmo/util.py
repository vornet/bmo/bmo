#!/usr/bin/python3

from __future__ import annotations
from dataclasses import InitVar, dataclass
from enum import Enum
from typing import Any, Type, TypeVar
import os


ItemT = TypeVar('ItemT')
TextSubT = TypeVar('TextSubT', bound='Text')


class TimeUnit(Enum):
    SECOND = 's'
    MINUTE = 'm'
    HOUR = 'h'
    DAY = 'd'
    WEEK = 'w'
    YEAR = 'y'

    def modulo(self, seconds: int) -> tuple[int, int]:
        minute = 60
        hour = minute * 60
        day = hour * 24
        week = day * 7
        year = day * 365.25
        size: dict[TimeUnit, float] = {
            TimeUnit.SECOND: 1,
            TimeUnit.HOUR: hour,
            TimeUnit.MINUTE: minute,
            TimeUnit.DAY: day,
            TimeUnit.WEEK: week,
            TimeUnit.YEAR: year,
        }
        if seconds < minute:
            return 0, seconds
        if self == self.SECOND:
            return seconds, 0
        if self not in size:
            raise ValueError(f"unsupported unit: {self}")
        remainder = seconds % size[self]
        amount = int((seconds - remainder) / size[self])
        return amount, int(remainder)


class _Void:
    pass


_VOID = _Void()


class ParsingError(ValueError):

    def __init__(self,
                 description: str | None = None,
                 loc: list[int | str] | None = None,
                 value: str | _Void = _VOID,
                 ):
        self.description = description
        self.loc = loc or []
        self.value = value


def sequence_duration(seconds: int) -> dict[TimeUnit, int]:
    """
    Split *seconds* duration to larger units, up to weeks

    Note that we avoid months like the plague.
    """
    rest = seconds
    parsed: dict[TimeUnit, int] = {}
    parsed[TimeUnit.YEAR], rest = TimeUnit.YEAR.modulo(rest)
    parsed[TimeUnit.WEEK], rest = TimeUnit.WEEK.modulo(rest)
    parsed[TimeUnit.DAY], rest = TimeUnit.DAY.modulo(rest)
    parsed[TimeUnit.HOUR], rest = TimeUnit.HOUR.modulo(rest)
    parsed[TimeUnit.MINUTE], rest = TimeUnit.MINUTE.modulo(rest)
    parsed[TimeUnit.SECOND] = rest
    return parsed


def fmt_duration(seconds: int) -> str:
    """
    Translate to a valid duration specification

    keep up to 2 largest non-zero units, but at least
    seconds if all else is zeroed
    """
    sequenced = sequence_duration(seconds)
    units = [TimeUnit.YEAR, TimeUnit.WEEK, TimeUnit.DAY, TimeUnit.HOUR, TimeUnit.MINUTE, TimeUnit.SECOND]
    parts: list[str] = []
    for unit in units:
        if not sequenced[unit]:
            continue
        parts.append(str(sequenced[unit]) + unit.value)
    return ','.join(parts)


def parse_duration_brief(expressions: list[str]) -> float:
    """
    Parse time duration text to number of seconds

    Each string in *expressions* must be in format:

        <NUMBER><UNIT>

    where NUMBER must be a number no lower than zero, and UNIT
    can be one of 's', 'm', 'h', 'd', 'w' or 'y'.  For example,
    '15m' is a valid EXPRESSION representing fifteen minutes.

    Individual expressions add up, so, eg. `["1h", "3m"]` will
    result in seconds corresponding to 63 minutes.

    UNIT represents 's' for second, 'm' for minute, 'h' for hourm
    'd' for day, 'w' for week and 'y' for year.  Months are not
    supported.  (They are evil, in case you haven't noticed.)
    """

    minute = 60
    hour = minute * 60
    day = hour * 24
    week = day * 7
    year = day * 365.25

    def parse_part(P):
        if P.endswith('s'):
            return float(P[:-1])
        elif P.endswith('m'):
            return float(P[:-1]) * minute
        elif P.endswith('h'):
            return float(P[:-1]) * hour
        elif P.endswith('d'):
            return float(P[:-1]) * day
        elif P.endswith('w'):
            return float(P[:-1]) * week
        elif P.endswith('y'):
            return float(P[:-1]) * year
        else:
            return float(P)
    total = 0
    for idx, part in enumerate(expressions):
        try:
            total += parse_part(part)
        except ValueError:
            raise ParsingError(value=part, loc=[idx])
    return total


def nlstrip(text: str) -> str:
    if text.endswith('\n'):
        return text[0:-1]
    return text


def nlsplit(text: str) -> list[str]:
    if not text:
        return []
    return nlstrip(text).split('\n')


def nljoin(lines: list[str]) -> str:
    return ''.join(line + '\n' for line in lines)


@dataclass(frozen=True, slots=False)
class Text:
    lines: list[str]
    _fail: InitVar[bool] = True

    def __post_init__(self, _fail: bool):
        if _fail:
            cls = self.__class__
            raise ValueError(
                f"don't instantiate {cls.__name__} directly;"
                f"use {cls.__name__}.from_*() or {cls.__name__}.new()"
            )

    @classmethod
    def new(cls: Type[TextSubT], value: Any = None) -> TextSubT:
        if value is None:
            return cls([], _fail=False)
        if type(value) is str:
            return cls.from_str(value)
        if type(value) is bytes:
            return cls.from_bytes(value)
        if type(value) is list:
            return cls(lines=value, _fail=False)
        if type(value) is cls:
            return cls(lines=value.as_lines, _fail=False)
        raise ValueError(
            f"value must be {cls.__name__}, str, bytes, list or None"
        )

    @classmethod
    def from_bytes(cls: Type[TextSubT], value: Any = None) -> TextSubT:
        if value is None:
            return cls.new()
        return cls.from_str(value.decode())

    @classmethod
    def from_lines(cls: Type[TextSubT], value: Any = None) -> TextSubT:
        if value is None:
            return cls.new()
        return cls(value, _fail=False)

    @classmethod
    def from_str(cls: Type[TextSubT], value: Any = None) -> TextSubT:
        if value is None:
            return cls.new()
        return cls(nlsplit(value), _fail=False)

    def __str__(self):
        return self.as_str

    def prefix(self, pfx: str) -> Text:
        return self.__class__.from_lines([
            pfx + line for line in self.lines
        ])

    @property
    def as_bytes(self) -> bytes:
        return self.as_str.encode()

    @property
    def as_lines(self) -> list[str]:
        return list(line for line in self.lines)

    @property
    def as_str(self) -> str:
        return nljoin(self.lines)


@dataclass(frozen=True, slots=False)
class TextFile:
    """
    Text file working with Text() objects
    """
    path: str

    def _ensure_dir(self) -> None:
        base, _ = self.path.rsplit('/', maxsplit=1)
        if os.path.isdir(base):
            return
        os.makedirs(base)

    def exists(self) -> bool:
        return os.path.isfile(self.path)

    def append(self, content: Any) -> None:
        txt = Text.new(content)
        self._ensure_dir()
        with open(self.path, 'a') as fh:
            fh.write(txt.as_str)

    def load(self) -> Text:
        if not self.exists():
            return Text.new()
        with open(self.path) as fh:
            return Text.from_str(fh.read())

    def save(self, content: Any) -> None:
        txt = Text.new(content)
        self._ensure_dir()
        with open(self.path, 'w') as fh:
            fh.write(txt.as_str)
