# places #

E.g.

    @home
    @work
    @public
    @...

# ambience #

Eg.

    =day
    =night


# interaction pattern #

can be qualified by note such as /lunch, /eng, /pto

E.g.

    %presenting      # strict dnd, display mirror...
    %working         # noooormallll...
    %watching        # dnd, no mon sleeping
    %mtg/comms       # mtg / call (mobile?)
    %lost/disconn    # hibernated / pulled the cord...
    %away            # lunch, training....
    %gone            # home, pto, ooo...

