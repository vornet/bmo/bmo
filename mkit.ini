[project]
    version     = 0.3.0
    name        = BMO
    tagline     = All you need for desktop ... except the drapes
    pkgname     = bmo
    maintainer  = Alois Mahdal <netvor+bmo@vornet.cz>
    vcs_browser = https://gitlab.com/vornet/bmo/bmo
    relsrc      = main
    reldst      = latest

[dist]
    tarball  = LICENSE.md
    tarball  = Makefile
    tarball  = README.md
    tarball  = mkit.ini
    tarball  = packaging
    tarball  = src
    tarball  = utils
    rpmstuff = packaging/template.spec
    debstuff = packaging/debian

[ENV]
    PREFIX          = /usr/local
    XDG_DATA_HOME   = $HOME/.local/share
    XDG_CACHE_HOME  = $HOME/.cache
    XDG_CONFIG_HOME = $HOME/.config
    PYVER3          = 3.X

[roots]
    bcompl  = /usr/share/bash-completion/completions
    bin     = [ENV:PREFIX]/bin
    doc     = [ENV:PREFIX]/share/doc/[project:pkgname]
    etc     = /etc/bmo
    libexec = [ENV:PREFIX]/libexec/[project:pkgname]
    sfincb  = [ENV:PREFIX]/share/shellfu/include-bash
    share   = [ENV:PREFIX]/share/[project:pkgname]
    pysite3 = [ENV:PREFIX]/lib/python[ENV:PYVER3]

[macros]
    __BMO_MAINDESC__ = BMO is a desktop helper toolkit to help with things like basic audio
    __BMO_MAINDESC__ = controls, URL bookmarking and opening, custom (d)menu construction, status
    __BMO_MAINDESC__ = switching and more.  What makes bmo special is that while being a deskop
    __BMO_MAINDESC__ = helper, it attempts to remain as desktop-agnostic as possible; in fact
    __BMO_MAINDESC__ = most of it should work perfectly even without a graphical server at all.
    __BMO_BINDESC__  = This package contains bmo meta-command and sub-commands.
    __BMO_MODDESC__  = This sub-package contains Shellfu/Bash modules used by bmo.
    __BMO_PYDESC__   = This sub-package contains Python modules used by bmo.
    __VDEP_PYTHON3_GE__  = 3.7
    __VDEP_PYTHON3_LT__  = 3.13
    __VDEP_SHELLFU_GE__  = 0.10.50
    __VDEP_SHELLFU_LT__  = 0.11.0
    __VDEP_SATURNIN_GE__ = 0.5.6
    __VDEP_SATURNIN_LT__ = 0.6.0
    __VDEP_PYSATURNIN_GE__ = 0.0.0
    __VDEP_PYSATURNIN_LT__ = 0.1.0
    __VDEP_PYCLAPP_GE__ = 0.0.0
    __VDEP_PYCLAPP_LT__ = 0.1.0

[build:macros]
    __SATURNIN_DATA_HOME__    = [ENV:XDG_DATA_HOME]/bmo
    __SATURNIN_CACHE_HOME__   = [ENV:XDG_CACHE_HOME]/bmo
    __SATURNIN_CONFIG_HOME__  = [ENV:XDG_CONFIG_HOME]/bmo
    __SATURNIN_BUILTINS__     = conf
    __SATURNIN_CONFIG_LOCAL__ = [roots:etc]
    __SATURNIN_LIBEXEC__      = [roots:libexec]
    __SATURNIN_SHARE__        = [roots:share]
    __SATURNIN_SCCOMPGEN_PATH__ = [roots:share]/complete
    __SATURNIN_MSGMODE__      = color
    __SATURNIN_VERBOSE__      = false

[modes]
    bin     = 755
    libexec = 755

[files]
    bcompl  = src/complete.bash                 bmo
    bin     = src/app                           bmo
    bin     = src/pyapp                         pybmo
    doc     = LICENSE.md
    doc     = src/doc/bmo-nag.md
    doc     = src/doc/bmo-menu.md
    doc     = src/doc/bmo-mka.md
    doc     = README.md
    libexec = src/libexec/bmo-au
    libexec = src/libexec/bmo-be
    libexec = src/libexec/bmo-clip
    libexec = src/libexec/bmo-dmenu
    libexec = src/libexec/bmo-kb
    libexec = src/libexec/bmo-menu
    libexec = src/libexec/bmo-mka
    libexec = src/libexec/bmo-nag
    libexec = src/libexec/bmo-sensible
    libexec = src/libexec/bmo-wish
    sfincb  = src/shellfu/bmo_be.sh
    sfincb  = src/shellfu/bmo_beh.sh
    sfincb  = src/shellfu/bmo_clip.sh
    share   = src/ini.d
    share   = src/help
    share   = src/complete
    pysite3 = src/python/bmo

#mkit version=0.1.3
