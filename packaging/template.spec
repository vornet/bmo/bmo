%global sfincb %{_datadir}/shellfu/include-bash
%global shellfu_req shellfu >= __VDEP_SHELLFU_GE__, shellfu < __VDEP_SHELLFU_LT__
%global python3_req python3 >= __VDEP_PYTHON3_GE__, python3 < __VDEP_PYTHON3_LT__
%global saturnin_req shellfu-bash-saturnin >= __VDEP_SATURNIN_GE__, shellfu-bash-saturnin < __VDEP_SATURNIN_LT__
%global pysaturnin_req python3-saturnin >= __VDEP_PYSATURNIN_GE__, python3-saturnin < __VDEP_PYSATURNIN_LT__
%global pyclapp_req python3-clapp >= __VDEP_PYCLAPP_GE__, python3-clapp < __VDEP_PYCLAPP_LT__
%if ( 0%{?rhel} && 0%{?rhel} < 7 ) || ( 0%{?centos} && 0%{?centos} < 7 )
%global pspkg procps
%else
%global pspkg procps-ng
%endif

Name:           __MKIT_PROJ_PKGNAME__
Version:        __MKIT_PROJ_VERSION__
Release:        1%{?dist}
Summary:        __MKIT_PROJ_NAME__ - __MKIT_PROJ_TAGLINE__
URL:            __MKIT_PROJ_VCS_BROWSER__
License:        GPLv2
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  %saturnin_req
BuildRequires:  %pysaturnin_req
BuildRequires:  %shellfu_req
BuildRequires:  make
BuildRequires:  python3-devel
BuildRequires:  shellfu-bash-pretty

Requires:   dmenu
Requires:   python3-jinja2
Requires:   %python3_req
Requires:   %shellfu_req
Requires:   shellfu-bash-pretty
Requires:   %saturnin_req
Requires:   %pyclapp_req
Requires:   %pysaturnin_req
Requires:   shellfu-bash-bmo == %{version}
Requires:   python3-bmo == %{version}
Requires:   python3-cpi
Requires:   python3-neaty
Requires:   python3-uripecker
Requires:   shellfu-sh-termcolors
Requires:   slock
Requires:   xclip

%description
__BMO_MAINDESC__

__BMO_BINDESC__

%package -n shellfu-bash-bmo
Summary: Shellfu/Bash modules for bmo
Requires: %pspkg
Requires: %shellfu_req
Requires: shellfu-bash
Requires: shellfu-bash-pretty
Requires: shellfu-sh-isa
%description -n shellfu-bash-bmo
__BMO_MODDESC__

%package -n python3-bmo
Summary: Python3 modules for bmo
Requires: python3-neaty
Requires: %python3_req
Requires: %pyclapp_req
Requires: %pysaturnin_req
%description -n python3-bmo
__BMO_PYDESC__

%prep
%setup -q

%build
make %{?_smp_mflags} PREFIX=/usr PYVER3=%{python3_version}

%install
%make_install PREFIX=/usr PYVER3=%{python3_version}
PYSITE3="%{buildroot}/%{python3_sitelib}"
mkdir -p "$PYSITE3"
mv "${PYSITE3%/site-packages}/%{name}" "$PYSITE3"

%files
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/ini.d
%dir %{_datadir}/%{name}/ini.d/main
%dir %{_datadir}/%{name}/ini.d/main/mka
%dir %{_datadir}/%{name}/ini.d/main/mka/templates
%dir %{_libexecdir}/%{name}
%doc %{_docdir}/%{name}/LICENSE.md
%doc %{_docdir}/%{name}/README.md
%doc %{_docdir}/%{name}/bmo-menu.md
%doc %{_docdir}/%{name}/bmo-mka.md
%doc %{_docdir}/%{name}/bmo-nag.md
%{_bindir}/%{name}
%{_bindir}/py%{name}
%{_datadir}/%{name}/complete/au.sh
%{_datadir}/%{name}/complete/be.sh
%{_datadir}/%{name}/complete/clip.sh
%{_datadir}/%{name}/complete/dmenu.sh
%{_datadir}/%{name}/complete/kb.sh
%{_datadir}/%{name}/complete/menu.sh
%{_datadir}/%{name}/complete/mka.sh
%{_datadir}/%{name}/complete/nag.sh
%{_datadir}/%{name}/complete/sensible.sh
%{_datadir}/%{name}/complete/wish.sh
%{_datadir}/%{name}/help
%{_datadir}/%{name}/ini.d/main/be.ini
%{_datadir}/%{name}/ini.d/main/mka.ini
%{_datadir}/%{name}/ini.d/main/mka/templates/email.eml
%{_datadir}/%{name}/ini.d/main/sensible.ini
%{_datadir}/bash-completion/completions/%{name}
%{_libexecdir}/%{name}/%{name}-au
%{_libexecdir}/%{name}/%{name}-be
%{_libexecdir}/%{name}/%{name}-clip
%{_libexecdir}/%{name}/%{name}-dmenu
%{_libexecdir}/%{name}/%{name}-kb
%{_libexecdir}/%{name}/%{name}-menu
%{_libexecdir}/%{name}/%{name}-mka
%{_libexecdir}/%{name}/%{name}-nag
%{_libexecdir}/%{name}/bmo-sensible
%{_libexecdir}/%{name}/bmo-wish

%files -n shellfu-bash-bmo
%{sfincb}/bmo_be.sh
%{sfincb}/bmo_beh.sh
%{sfincb}/bmo_clip.sh

%files -n python3-bmo
%{python3_sitelib}/%{name}/

%changelog

# specfile built with MKit __MKIT_MKIT_VERSION__
