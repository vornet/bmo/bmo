# mkit - Simple Makefile target helper
# See LICENSE file for copyright and license details.

export MKIT_DIR

plugins := $(shell "$(MKIT_DIR)"/mkit --list-plugins)


all: build

_mkit_builddata: _mkit_metadata
	@"$(MKIT_DIR)"/mkit _mkit_builddata

_mkit_inidata:
	@"$(MKIT_DIR)"/mkit _mkit_inidata

_mkit_metadata:
	@"$(MKIT_DIR)"/mkit _mkit_metadata

_mkit_show_metadata: _mkit_metadata
	@"$(MKIT_DIR)"/mkit _mkit_show_metadata

_mkit_show_builddata: _mkit_builddata
	@"$(MKIT_DIR)"/mkit _mkit_show_builddata

build: _mkit_builddata
	@"$(MKIT_DIR)"/mkit build

dist: _mkit_metadata
	@"$(MKIT_DIR)"/mkit dist

clean:
	@"$(MKIT_DIR)"/mkit clean

install: all
	@"$(MKIT_DIR)"/mkit install

release:
	@"$(MKIT_DIR)"/mkit release

release_x:
	@"$(MKIT_DIR)"/mkit release_x

release_y:
	@"$(MKIT_DIR)"/mkit release_y

release_z:
	@"$(MKIT_DIR)"/mkit release_z

uninstall:
	@"$(MKIT_DIR)"/mkit uninstall

vbump:
	@"$(MKIT_DIR)"/mkit vbump

vbump_x:
	@"$(MKIT_DIR)"/mkit vbump_x

vbump_y:
	@"$(MKIT_DIR)"/mkit vbump_y

vbump_z:
	@"$(MKIT_DIR)"/mkit vbump_z

$(plugins): clean _mkit_metadata
	@"$(MKIT_DIR)"/mkit $@

.PHONY: all _mkit_builddata _mkit_inidata _mkit_metadata _mkit_show_builddata _mkit_show_metadata clean dist rpmstuff install uninstall release release_x release_y release_z vbump vbump_x vbump_y vbump_z $(plugins)
