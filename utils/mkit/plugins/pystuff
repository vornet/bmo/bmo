#!/bin/bash
# MKit - simple install helper
# See LICENSE file for copyright and license details.

mkit_import build
mkit_import target

pystuff__main() {
    #
    # Build setup.py and mkvenv.sh
    #
    local ModName           # Python module name
    local ProjVersion       # MKit project version
    local Skel              # setup.py skeleton
    local MkvenvDir         # venv maker: target venv directory
    local MkvenvScript      # venv maker: script name
    local MkvenvPost        # venv maker: post commands
    target__run build || die
    Skel="$(ini 1value "dist:pystuff")"
    MkvenvDir=$(__pystuff__load_mkvenv_dir)
    MkvenvScript=$(__pystuff__load_mkvenv_script)
    MkvenvPost=$(__pystuff__load_mkvenv_post)
    ProjVersion=$(build__cached semver)
    ModName=$(ini 1value macros:__VDK_PYLIB_MODNAME__)
    build__fact "pystuff/mkvenv_dir" <<<"$MkvenvDir"
    build__fact "pystuff/mkvenv_script" <<<"$MkvenvScript"
    test -n "$Skel" || die "dist:pystuff not specified"
    test -f "$Skel" || die "setup.py template not found: $Skel"
    __pystuff__make_setup_py
    __pystuff__make_mkvenv
}

__pystuff__find_dist() {
    local candidate
    candidate=$(
        find "dist" -maxdepth 1 -mindepth 1 -printf '%P\n' \
          | grep -m1 -e "$ModName-.*tar.gz"
    )
    test -f "dist/$candidate" && echo "dist/$candidate" && return 0
    #shellcheck disable=SC2016
    warn "could not find dist package in: dist" \
         " .. hint: looking for $ModName-*.tar.gz" \
         ' .. hint: did you run `make pystuff` first?'
    return 3
}

__pystuff__make_setup_py() {
    build__file "$Skel" "setup.py" pystuff
    build__recordr dist
    build__recordr "$ModName.egg-info"
    python3 -m build
}

__pystuff__load_mkvenv_dir() {
    #
    # Safely load directory for the venv
    #
    util__loadenv PYSTUFF__VENV_DIR && return 0
    plugin__option_single "dir" && return 0
    echo venv
}

__pystuff__load_mkvenv_script() {
    #
    # Safely load command for the venv
    #
    util__loadenv PYSTUFF__VENV_SCRIPT && return 0
    plugin__option_single "mkvenv_script" && return 0
    echo mkvenv.sh
}

__pystuff__load_mkvenv_post() {
    #
    # Safely load command for the venv
    #
    util__loadenv PYSTUFF__VENV_POST && return 0
    plugin__option_multi "mkvenv_post" && return 0
}

__pystuff__q() {
    #
    # Quote command $@
    #
    printf '%q ' "$@";
    echo
}

__pystuff__make_mkvenv() {
    #
    # Create the mkvenv.sh script
    #
    local dist
    local req
    dist=$(__pystuff__find_dist) \
     || die "could not find dist"
    build__record "$MkvenvScript"
    {
        echo "#!/bin/sh -e"
        echo "#"
        echo "# created by MKit $MKIT_VERSION pystuff for $ModName-$ProjVersion"
        echo "#"
        echo ""
        __pystuff__q python3 -m venv "$MkvenvDir"
        __pystuff__q . "$MkvenvDir/bin/activate"
        python3 "setup.py" --requires \
          | grep . \
          | while read -r req; do
                __pystuff__q python3 -m pip install --force-reinstall "$req"
            done
        __pystuff__q python3 -m pip install --force-reinstall "$dist"
        echo ""
        echo "echo >&2 'Virtual environment should be ready. run following command:'"
        echo "echo >&2 ''"
        echo "echo >&2 '    source venv/bin/activate'"
        echo "echo >&2 ''"
        echo "echo >&2 'to activate it.'"
        test -n "$MkvenvPost" && {
            echo
            echo "#"
            echo "# user POST action ([options:pystuff:mkvenv_post]"
            echo "#"
            echo "$MkvenvPost"
        }
    } | sed 's/ *$//' >"$MkvenvScript"
    bash -n "$MkvenvScript" || die "script has syntax errors"
    chmod +x "$MkvenvScript"
}
